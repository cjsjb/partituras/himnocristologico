% This LilyPond file was generated by Rosegarden 1.7.3
\version "2.10.0"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)
\header {
	title = "Himno cristológico"
	tagline = "Coro Juvenil San Juan Bosco"
	composer = "Alejandro Mejía Pereda"
	instrument = "Soprano"
}
#(set-global-staff-size 20)
#(set-default-paper-size "letter")
\paper {
	#(define line-width (* 7 in))
	print-first-page-number = ##t
	ragged-bottom = ##t
	first-page-number = 1
}
global = {
	\time 6/8
}
globalTempo = {
	\tempo 4. = 113	\skip 2.*48
	\tempo 4. = 93	\skip 2.*7
	\tempo 4. = 113
}
\score {
	<<
		% force offset of colliding notes in chords:
		\override Score.NoteColumn #'force-hshift = #1.0

		\include "himnocristologico-acordes.inc"
		\include "himnocristologico-soprano.inc"
	>>
}
