\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key e \minor

		R2.*4  |
%% 5
		e 2.  |
		b 2 b 4  |
		a 2 a 4  |
		g 4 fis e  |
		d 4 d d  |
%% 10
		fis 4 e d  |
		e 2. ~  |
		e 4 r8 r4.  |
		e 2 e 4  |
		b 2 b 4  |
%% 15
		cis' 2 cis' 4  |
		a 4 fis a  |
		b 4 cis' b  |
		a 4 fis a  |
		b 2. ~  |
%% 20
		b 4 r8 r4.  |
		e 2.  |
		b 2 b 4  |
		a 2 a 4  |
		g 4 fis e  |
%% 25
		d 4 d d  |
		fis 4 e d  |
		e 2. ~  |
		e 4 r8 r4.  |
		e 2 e 4  |
%% 30
		b 2 b 4  |
		cis' 2 cis' 4  |
		a 4 fis a  |
		b 4 cis' b  |
		a 4 fis a  |
%% 35
		b 2. ~  |
		b 4 r8 r4.  |
		g 2.  |
		g 4 fis e  |
		fis 2.  |
%% 40
		d 2 r8 r  |
		e 2.  |
		fis 2 e 4  |
		fis 2. ~  |
		fis 4 r8 r4.  |
%% 45
		g 2.  |
		g 4 fis e  |
		fis 2.  |
		d 2 r8 r  |
		cis 4 cis e  |
%% 50
		fis 4 e cis  |
		d 2. ~  |
		d 4 r8 r4.  |
		cis 4 cis e  |
		fis 4 e cis  |
%% 55
		d 2. ~  |
		d 4 r8 r4.  |
		e 2.  |
		b 2 b 4  |
		a 2 a 4  |
%% 60
		g 4 fis e  |
		d 4 d d  |
		fis 4 e d  |
		e 2. ~  |
		e 4 r8 r4.  |
%% 65
		e 2 e 4  |
		b 2 b 4  |
		cis' 2 cis' 4  |
		a 4 fis a  |
		b 4 cis' b  |
%% 70
		a 4 fis a  |
		b 2. ~  |
		b 4 r8 r4.  |
		e 2.  |
		b 2 b 4  |
%% 75
		a 2 a 4  |
		g 4 fis e  |
		d 4 d d  |
		fis 4 e d  |
		e 2. ~  |
%% 80
		e 4 r8 r4.  |
		e 2 e 4  |
		b 2 b 4  |
		cis' 2 cis' 4  |
		a 4 fis a  |
%% 85
		b 4 cis' b  |
		a 4 fis 2 -\fermata  |
		a 2.  |
		b 2. ~  |
		b 2.  |
%% 90
		R2.  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Cris -- to Je -- sús, el cual e -- xis -- tí -- "a en" la for -- ma de Dios __
		"no e" -- xi -- gió te -- ner la glo -- ria de -- bi -- "da a" su di -- vi -- ni -- dad. __
		"Se a" -- no -- na -- dó to -- man -- do la for -- ma del sier -- vo de Dios __
		y "se a" -- se -- me -- jó a to -- dos los hom -- bres en su con -- di -- ción. __

		Ha -- cién -- do -- se hom -- bre se hu -- mi -- lló, __
		se hi -- "zo o" -- be -- dien -- te
		has -- ta mo -- rir en la cruz, __
		has -- ta mo -- rir en la cruz. __

		Por e -- so Dios de mo -- "do ad" -- mi -- ra -- ble  a Cris -- "to e" xal -- tó __
		y le o -- tor -- gó un nom -- bre tan al -- to "que a" to -- "do ex" -- ce -- dió. __
		Pa -- ra "que a" -- sí el cos -- mos en -- te -- ro se cen -- "tre en" Je -- sús. __
		Él es el Se -- ñor "que a" to -- dos con -- du -- ce al Pa -- dre.
		A -- mén. __
	}
>>
