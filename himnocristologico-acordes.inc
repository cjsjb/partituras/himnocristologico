\context ChordNames
	\chords {
		\set chordChanges = ##t
		% intro
		e2.:m e2.:m e2.:m e2.:m

		% cristo jesus...
		e2.:m e2.:m a2. a2.
		d2. b2.:m e2.:m e2.:m
		e2.:m e2.:m a2. d2.
		b2.:m fis2.:m b2.:m
		b2.:7

		% se anonado...
		e2.:m e2.:m a2. a2.
		d2. b2.:m e2.:m e2.:m
		e2.:m e2.:m a2. d2.
		b2.:m fis2.:m b2.:m
		b2.:m

		% haciendose hombre...
		e2.:m e2.:m b2.:m b2.:m
		fis2.:7 fis2.:7 b2.:m b2.:m
		e2.:m e2.:m b2.:m b2.:m
		fis2.:7 fis2.:7 b2.:m b2.:m
		fis2.:7 fis2.:7 b2.:m b2.:7

		% por eso dios...
		e2.:m e2.:m a2. a2.
		d2. b2.:m e2.:m e2.:m
		e2.:m e2.:m a2. d2.
		b2.:m fis2.:m b2.:m
		b2.:7

		% para que asi...
		e2.:m e2.:m a2. a2.
		d2. b2.:m e2.:m e2.:m
		e2.:m e2.:m a2. d2.
		b2.:m fis2.:m r2.
		b2.:m b2.:m
	}
